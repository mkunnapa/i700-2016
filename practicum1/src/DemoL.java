/**
 * Created by mkunnapa on 29.08.2016.
 */
public class DemoL {

    public static void main(String[] args) {
        System.out.println(myRealAge());

    }

    public static int myAgeHere() {
        return 100;
    }

    public static int myRealAge() {
        return myAgeHere()+100;
    }

}
