package recursion;

import java.util.Timer;

/**
 * Created by mkunnapa on 31.10.2016.
 */
public class RecursionDemo {
    public static void main(String[] args) {
        //greet();
        Timer timer = new Timer();
        long timeBefore = System.currentTimeMillis();
//        System.out.println(fib(45));
        System.out.println(factorial(20));
        System.out.println("Time spent: " + (System.currentTimeMillis() - timeBefore) + "ms");

    }

    private static void greet() {
        System.out.println("Hello");
        greet();
    }

    private static int fib(int n) {
        if (n <= 1) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }

    private static long factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return n * factorial(n - 1);
    }


}
