package conditionals;

/**
 * Created by mkunnapa on 12.09.2016.
 */
public class DemoSwitch {
    public static void main(String[] args) {
        int condition = 1;

        switch (condition) {
            case 1:
            System.out.println("MY abc");
            break;

            case 2:
            System.out.println("MY abd");
            break;

            default:
                System.out.println("MY abe");
                break;

        }
    }
}
