package conditionals;

import lib.TextIO;

/**
 * Created by mkunnapa on 12.09.2016.
 */
public class Demo {
    public static void main(String[] args) {

        boolean myBool = true;

        if (myBool) {
            int x = 3;
        } else {
            System.out.println();
        }

    }

}
