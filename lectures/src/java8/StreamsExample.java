package java8;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Mihkel on 06.11.2016. Based on tutorialspoint java 8 tutorial
 */
public class StreamsExample {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println("===Non empty strings     stream()===");
        filtered.forEach(System.out::println);

        Random random = new Random();
        System.out.println("===10 random numbers        foreach(), limit()===");
        random.ints().limit(10).forEach(System.out::println);

        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        List<Integer> squaresList = numbers.stream().map(i -> i*i).distinct().collect(Collectors.toList());
        System.out.println("===Unique squares       map()===");
        squaresList.forEach(System.out::println);

        Random random2 = new Random();
        int i = random2.nextInt((500 - 1) + 1) + 1;
        System.out.println("1 random value: " + i);
        System.out.println("===10 sorted random doubles     sorted() ===");
        random2.doubles().limit(10).sorted().forEach(System.out::println);
        System.out.println("===10 sorted random numbers     sorted() ===");
        random2.ints().limit(10).sorted().forEach(System.out::println);

        String mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining(", "));
        System.out.println("===Merged strings       Collectiors.joining()===");
        System.out.println("Merged String: " + mergedString);


        List<Integer> numbers2 = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        IntSummaryStatistics stats = numbers2.stream().mapToInt((x) -> x).summaryStatistics();
        System.out.println("===Statistics===");
        System.out.println("Highest number in List : " + stats.getMax());
        System.out.println("Lowest number in List : " + stats.getMin());
        System.out.println("Sum of all numbers : " + stats.getSum());
        System.out.println("Average of all numbers : " + stats.getAverage());

    }
}
