package java8;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Mihkel on 06.11.2016. Based on Oracle Java SE 8: Lambda Quick Start && tutorialspoint java 8 tutorial
 */
public class ComparatorExample {

    public static void main(String[] args) {
        List<Person> aTeam = Person.callATeam();

        // Sort with Inner Class
        Collections.sort(aTeam, new Comparator<Person>() {
            public int compare(Person p1, Person p2) {
                return p1.getSurName().compareTo(p2.getSurName());
            }
        });

        System.out.println("=== Sorted Asc SurName ===");
        for (Person p : aTeam) {
            p.printName();
        }

        // Use Lambda instead

        // Print Asc
        System.out.println("=== Sorted Asc SurName ===");
        Collections.sort(aTeam, (Person p1, Person p2) -> p1.getSurName().compareTo(p2.getSurName()));

        for (Person p : aTeam) {
            p.printName();
        }

        // Print Desc
        System.out.println("=== Sorted Desc SurName ===");
        Collections.sort(aTeam, (p1, p2) -> p2.getSurName().compareTo(p1.getSurName()));

        for (Person p : aTeam) {
            p.printName();
        }

        //Method reference
        System.out.println("=== Method reference ===");
        aTeam.forEach(Person::printName);

    }
}
