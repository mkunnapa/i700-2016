package loops;


public class LoopsDemo {
    public static void main(String[] args) {
        //If statement
//        int x = 3;
//        if (x > 4) {
//            System.out.println("X is less than four");
//        }

        //While statement
//        int x = 5;
//        while (x < 4) {
//            System.out.println("X is " + x);
//            x = x + 1;
//        }

        //For statement
//        for (int i = 1; i < 4; i++) {
//            System.out.println("I is " + i);
//        }

//        for (int i = 0; i < 7; i++) {
//            System.out.println("I will be written out 7 times");
//        }

//        int var1 = 123;
//        while (var1 % 2 != 0) {
//            var1-=11;
//        }
//        System.out.println(var1);

        //Foreach
//        int[] myIntegers = {1,2,3,4,5,6};
//        for (int myInteger: myIntegers) {
//            System.out.println(myInteger * 2);
//        }
//
//        System.out.println("=====");
//
//        for (int i = 0; i < myIntegers.length; i++) {
//            System.out.println(myIntegers[i] * 3);
//        }

        //Do-while
//        int y = 2;
//        do {
//            System.out.println("Y is " + y);
//            y = y + 1;
//        } while (y < 4);

        //Break-continue
//        int z = 0;
//        while (true) {
//            z++;
//            System.out.println(z);
//            if (z == 136) {
//                break;
//            }
//        }

//        for (int i = 0; i < 5; i++) {
//            if (i == 3) {
//                continue;
//            }
//            System.out.println(i);
//        }

        int var2 = 3;
        whileLoop:
        while (true) {
            var2 += 10;
            if (var2 == 33) {
                continue whileLoop;
            }

            if (var2 == 103) {
                break whileLoop;
            }
            System.out.println(var2);

        }

        //First-Last
        //boolean first = true;
//        int amount = 10;
//        for (int i = 0; i < amount; i++) {
//            if (i == 0) {
//                System.out.println("First time");
////                first = false;
//            } else if (i == amount-1) {
//                System.out.println("Last time");
//            } else {
//                System.out.println(i);
//            }
//
//        }

        //Loop within loop
//        for (int row = 0; row < 10; row++) {
//            for (int col = 0; col < 10; col++) {
//                if (row == 3) {
//                    System.out.print("= ");
//                } else {
//                    System.out.print(col + " ");
//                }
//            }
//            System.out.println();
//        }

//        for (int row = 1; row <= 10; row++) {
//            for (int col = 1; col <= 10; col++) {
//                System.out.print(row * col + " ");
//            }
//            System.out.println();
//        }
    }
}
