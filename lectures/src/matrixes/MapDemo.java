package matrixes;

import oop.Sportsman;

import java.util.*;

/**
 * Created by mkunnapa on 31.10.2016.
 */
public class MapDemo {
    public static void main(String[] args) {
        Map<String, Sportsman> sportsmanMap = new LinkedHashMap<>();
        Sportsman sportsman = new Sportsman();
        sportsman.setName("Mark");
        //sportsmanMap.put("smth", sportsman);
        Sportsman sportsman2 = new Sportsman();
        sportsman2.setName("Nameless");
        //sportsmanMap.put("a", sportsman2);
        //sportsmanMap.put("smthElse", new Sportsman());

        ArrayList<Sportsman> competitors = new ArrayList<>();
        competitors.add(sportsman);
        competitors.add(sportsman2);

        sportsmanMap.put("f812a", sportsman);
        sportsmanMap.put("h12zs", sportsman2);

        //stuff gets done

        competitors.get(0);
        sportsmanMap.get("f812a");




        Set<Map.Entry<String, Sportsman>> entries = sportsmanMap.entrySet();
        Iterator<Map.Entry<String, Sportsman>> iterator = entries.iterator();

        while (iterator.hasNext()) {
            iterator.next();
        }

        List<Sportsman> newList = new ArrayList<>();
        newList.add(sportsman);
        newList.add(sportsman2);

        Iterator<Sportsman> listIterator = newList.iterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().getName());
        }
        /*for (Sportsman sportsman1 : sportsmanMap.values()) {
            System.out.println(sportsman1.getName());
        }
        ;*/
        /*Sportsman smth = sportsmanMap.get("smth");
        System.out.println(smth.getName());*/


    }
}
