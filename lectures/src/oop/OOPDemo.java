package oop;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public class OOPDemo {

    public static void main(String[] args) {
        Shape myShape = new Circle();
        myShape.getColor();
        List<Integer> myList = new ArrayList<Integer>();
        List<Integer> anotherList = new LinkedList<>();

        Weekday today = Weekday.MONDAY;
        Weekday another = Weekday.valueOf("TUESDAY");
        System.out.println(today.getDayNumber());
    }
}
