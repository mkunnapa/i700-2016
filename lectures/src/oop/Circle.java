package oop;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public class Circle extends Shape {
    double radius;

    @Override
        double area() {
        return radius * radius * Math.PI;
    }
}
