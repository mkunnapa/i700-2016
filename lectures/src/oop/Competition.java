package oop;

import java.util.ArrayList;

public class Competition {

    public static void main(String[] args) {
//        ArrayList<String> names = new ArrayList<>();
//        ArrayList<Double> results = new ArrayList<>();
//
//        names.add("John");
//        results.add(13.2);
//
//        results.remove(0);
//
//        names.get(0);
//        results.get(0);
        ArrayList<Human> sportsmen = new ArrayList<>();
        Sportsman sportsman1;
        sportsman1 = new Sportsman();
        sportsman1.setName("John");
        sportsman1.setBirthYear(1990);
        sportsman1.addResult(13.2);
        sportsmen.add(sportsman1);

        Sportsman sportsman2 = new Sportsman();
        sportsman2.setName("Jane");
        sportsman2.setBirthYear(1985);
        sportsman2.addResult(15.3);
        sportsmen.add(sportsman2);

        for (Human sportsman : sportsmen) {
            System.out.println(sportsman.getName() + " " + sportsman.getAge());
            System.out.println("Current world record is: " + Sportsman.worldRecordResult);
        }


    }
}
