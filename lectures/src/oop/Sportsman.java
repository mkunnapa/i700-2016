package oop;

import java.util.ArrayList;

public class Sportsman extends Human{
    static double worldRecordResult;

    private ArrayList<Double> results = new ArrayList<>();

/*    public Sportsman () {
        System.out.println("We are in the constructor");
    }*/

    public void addResult(double result) {
        this.results.add(result);
        if (result > worldRecordResult) {
            worldRecordResult = result;
        }
    }

    @Override
    public int getAge() {
        return 21;
    }
}
