package oop;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public enum Weekday {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);

    private int dayNumber;

    Weekday(int num) {
        dayNumber = num;
    }

    public int getDayNumber() {
        return this.dayNumber;
    }
}
