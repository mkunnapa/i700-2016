package graphics;

import java.applet.Applet;
import java.awt.*;

/**
 * Created by Mihkel on 22.10.2016.
 */
public class AppletDemo extends Applet {

    int left = 10;

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(left,this.getHeight() / 2,100,100);
        left+=10;
    }
}
