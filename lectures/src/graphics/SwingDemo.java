package graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Taken from http://www.sw-engineering-candies.com/snippets/java/hello-world-java-with-swing
 */
public class SwingDemo {
    public static void main(final String[] args) {
        final SwingDemo app = new SwingDemo();
        app.buildAndDisplayGui();
    }

    private void buildAndDisplayGui() {
        final JFrame frame = new JFrame("Minimal Swing Application");
        buildContent(frame);
        frame.setMinimumSize(new Dimension(300, 180));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocation(400, 300);
        frame.setVisible(true);
    }

    private void buildContent(final JFrame aFrame) {
        final JPanel panel = new JPanel();
        panel.add(new JLabel("Please press the button:"));

        final JButton okButton = new JButton("OK");
        okButton.addActionListener(new ShowDialog(aFrame));
        panel.add(okButton);

        aFrame.getContentPane().add(panel);
    }

    private static final class ShowDialog implements ActionListener {

        ShowDialog(final JFrame aFrame) {
            fFrame = aFrame;
        }

        @Override
        public void actionPerformed(final ActionEvent aEvent) {
            JOptionPane.showMessageDialog(fFrame, "Hello World");
        }

        private final JFrame fFrame;
    }
    /*public static void main(String[] args) {
        JFrame frame = new JFrame("HelloFrame");
        JPanel panel = new JPanel();
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setVisible(true);

        JButton button = new JButton("Click me!");
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Well hello there!");
            }
        });
        panel.add(button);

    }*/
}
