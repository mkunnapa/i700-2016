package arrays;


import lib.TextIO;
import methods.MethodsDemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class ArraysDemo {
    public static void main(String[] args) {
        printingAnArray();

//        String[] names = new String[5];
//        names[0] = "John";
//        arrayListDemo();
//        System.out.println(listDemo2());

        String[] names = arrayOfStrings();
        for (String name :
                names) {
            System.out.println(name);
        }
    }

    private static void printingAnArray() {
        int[] numbers = {3, 5, 8, 19};
        System.out.println(numbers[4]);
        System.out.println(numbers);
        Arrays.toString(numbers);

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        for (int number : numbers) {
            System.out.println(number);
        }
    }

    public static void getNames() {
        String[] names = new String[5];

        for (int i = 0; i < names.length; i++) {
            System.out.println("Input a name");
            names[i] = TextIO.getln();
        }

        java.util.Arrays.sort(names);
        for (String name: names) {
            System.out.println(name);
        }
    }

    //ArrayList demo (declare, add, remove, print out, if statement, get
    public static void arrayListDemo() {
        LinkedList<Integer> numbers = new LinkedList<>();
        numbers.add(8);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);

        System.out.println(numbers);

        numbers.remove(1);

        System.out.println(numbers);

        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) == 4) {
                numbers.set(i, -10);
            }
        }

        System.out.println(numbers);

        Collections.sort(numbers);

        System.out.println(numbers);
    }

    //ArrayList sort
    public static ArrayList<String> listDemo2() {
        ArrayList<String> names = new ArrayList<String>();

        for (int i = 0; i < 6; i++) {
            System.out.println("Enter a name");
            names.add(TextIO.getln());
        }

        Collections.sort(names);
        System.out.println(names);
        names.remove(3);

        return names;
    }

    public static String[] arrayOfStrings() {
        String[] names = new String[6];

        for (int i = 0; i < names.length; i++) {
            System.out.println("Enter a name:");
            names[i] = TextIO.getln();
        }

        Arrays.sort(names);

        return names;
    }

}
