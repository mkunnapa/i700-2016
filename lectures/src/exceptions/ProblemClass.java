package exceptions;

import java.util.ArrayList;

/**
 * Created by mkunnapa on 24.10.2016.
 */
public class ProblemClass {
    public static void main(String[] args) {
        finallyMethod(0);
        /*ArrayList<String> myList = new ArrayList<>();
        String someValue;
        try {
            someValue = myList.get(3);
            System.out.println("anything");
        } catch (IndexOutOfBoundsException e) {
            someValue = "Fake value";
        } finally {

        }*/


        /*System.out.println("Hello exceptions");
        try {
            tempMethod();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("I am after the exception");*/
    }

    public static void tempMethod() throws Exception {
        problemMethod();
    }

    public static void problemMethod() throws Exception {
        System.out.println("Something");
        throw new Exception("I am an exception");
    }

    public static void finallyMethod(int divideBy) {
        try {
            int d = 10 / divideBy;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("You can't divide by zero, dummy");
        } finally {
            System.out.println("We managed to do something");
        }
    }

    public static int getPositiveNumberFromUser() {
        //we get input from the user
        /*if(input < 0) {
            throw new NumberSmallerThanZeroException();
        }*/


        return -1;
    }
}
