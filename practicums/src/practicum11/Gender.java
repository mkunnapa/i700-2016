package practicum11;

/**
 * Created by Mihkel on 06.11.2016. Based on Java SE 8: Lambda Quick Start
 */
public enum Gender {
    MALE, FEMALE
}
