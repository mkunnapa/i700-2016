package practicum11;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mihkel on 06.11.2016.
 */
public class ATeam {
    static ArrayList<Person> aTeam;

    public static void main(String[] args) {
        aTeam =  Person.callATeam();
        foundAToolshed();
        System.out.println("Average age of members is: " + averageAge());
        List<Person> planePeople = peopleAwakeOnThePlane();
        planePeople.stream()
                .map(person -> person.geteMail())
                .forEach(System.out::println);
        greetRandomATeamMember();
    }

    private static void greetRandomATeamMember() {
        Random random = new Random();
        int index = random.nextInt(aTeam.size());
        Person person = aTeam.get(index);
        System.out.print("Hello, ");
        person.printName();
    }

    private static void foundAToolshed() {
        String namesString =
                aTeam.stream()
                        .filter(person -> person.getGender().equals(Gender.MALE))
                        .map(person -> person.getGivenName())
                        .collect(Collectors.joining(", "));

        String output = "Hey " + namesString + " I found a toolshed with welding equipment in it!";
        System.out.println(output);
    }

    private static double averageAge() {
        return aTeam.stream()
                .map(person -> person.getAge())
                .mapToInt((x) -> x)
                .summaryStatistics()
                .getAverage();
    }

    private static List<Person> peopleAwakeOnThePlane() {
        return aTeam.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE) || person.getAge() != 33)
                .collect(Collectors.toList());
    }





}
