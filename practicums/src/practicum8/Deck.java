package practicum8;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public class Deck {
    private ArrayList<Card> cards = new ArrayList<>();

    Deck () {
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                Card card = new Card(rank, suit);
                cards.add(card);
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    public Card draw() {
        return cards.remove(0);
    }
}
