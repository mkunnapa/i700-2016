package practicum8;

import lib.TextIO;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public class Test {
    public static void main(String[] args) {
        encrypt();
    }

    private static void encrypt() {
        System.out.println("Insert something");
        String input = TextIO.getlnString();
        String encrypted = DigestUtils.md5Hex(input);
        System.out.println(encrypted);
    }

    private static void decrypt() {
        final String EXPECTED = "7af8b0bdc404617d1d09f6f7b69498fb";
        final String INPUT = "abcdefghijklmnopqrstuvwxyz0123456789";

        if  (DigestUtils.md5Hex("xxxx").equals(EXPECTED)) {
            System.out.println("The initial string was xxxx");
        }
    }


}
