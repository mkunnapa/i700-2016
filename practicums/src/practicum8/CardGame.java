package practicum8;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by mkunnapa on 17.10.2016.
 */
public class CardGame {
    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();

        Hand hand = new Hand();
        for (int i = 0; i < 5; i++) {
            hand.drawFromDeck(deck);
        }

        hand.showCards();
        hand.checkForPokerHand();
        System.out.println("You have a " + hand.getHighestRank());
    }

}
