package practicum8;

/**
 * Created by Mihkel on 17.10.2016.
 */
public enum PokerRank {
    ROYAL_FLUSH,
    STRAIGHT_FLUSH,
    FOUR_OF_A_KIND,
    FULL_HOUSE,
    FLUSH,
    STRAIGHT,
    THREE_OF_A_KIND,
    TWO_PAIRS,
    PAIR,
    HIGH_CARD;

    @Override
    public String toString() {
        String stringValue = super.toString();
        return stringValue.charAt(0) + stringValue.substring(1).toLowerCase().replace('_',' ');
    }
}
