package practicum6;

import lib.TextIO;
import practicum5.Exercises;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayExercises {
    public static void main(String[] args) {
//        randomName();
//        randomNameWithArrayList();
//        reverse10Numbers();
//        reverse10UsingArrayList();

        if (args.length > 0) {
            System.out.println(args[0]);
        }

        numberOfAsInNames();
    }

    //random name from x names
    public static void randomName() {
        System.out.println("How many names do you want to enter?");
        int numberOfNames = TextIO.getInt();

        String[] names = new String[numberOfNames];

        for (int i = 0; i < numberOfNames; i++) {
            System.out.println("Please enter the name number " + (i + 1));
            names[i] = TextIO.getWord();
        }

        int randomNumber = Exercises.random(0, numberOfNames-1);

        System.out.println(names[randomNumber]);
    }

    public static void randomNameWithArrayList() {
        System.out.println("How many names do you want to enter?");
        int numberOfNames = TextIO.getInt();

        ArrayList<String> names = new ArrayList<>();

        for (int i = 0; i < numberOfNames; i++) {
            System.out.println("Please enter the name number " + (i + 1));
            names.add(TextIO.getWord());
        }

        int randomNumber = Exercises.random(0, numberOfNames - 1);

        System.out.println(names.get(randomNumber));
    }

    //numbers reverse order
    public static void reverse10Numbers() {
        int[] numbers = new int[10];

        for (int i = 0; i < 10; i++) {
            System.out.println("Enter number: ");
            numbers[i] = TextIO.getInt();
        }

        for (int i = (numbers.length - 1); i >= 0; i--) {
            System.out.println(numbers[i]);
        }

        int[] reversed = reverseNumbers(numbers);
        //reverse

        for (int i = 0; i < reversed.length; i++) {
            System.out.println(reversed[i]);
        }
    }

    public static void reverse10UsingArrayList() {
        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            System.out.println("Enter number: ");
            numbers.add(TextIO.getInt());
        }

        Collections.reverse(numbers);
        System.out.println(numbers);
    }

    public static int[] reverseNumbers(int[] arrayToReverse) {
        int[] reversedArray = new int[arrayToReverse.length];
        for (int i = 0; i < arrayToReverse.length; i++) {
            reversedArray[arrayToReverse.length - 1 - i] = arrayToReverse[i];
        }
        return reversedArray;
    }

    //ReadName from cmd
    //See practicum recording

    //number of a in names
    public static void numberOfAsInNames() {
        ArrayList<String> names = new ArrayList<>();

        boolean isEmpty = false;
        while (!isEmpty) {
            System.out.println("Enter a name");
            String name = TextIO.getln();
            if (name.isEmpty()) {
                isEmpty = true;
            } else {
                names.add(name);
            }
        }

        for (String name: names) {
            int amountOfAs = 0;
            for (int i = 0; i < name.length(); i++) {
                if (name.charAt(i) == 'a' || name.charAt(i) == 'A') {
                    amountOfAs++;
                }
            }
            System.out.println(amountOfAs + " " + name);
        }
    }
}
