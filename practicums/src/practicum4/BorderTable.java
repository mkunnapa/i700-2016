package practicum4;

import lib.TextIO;

public class BorderTable {
    public static void main(String[] args) {
        /*
        -----------------
        | x 0 0 0 0 0 x |
        | 0 x 0 0 0 x 0 |
        | 0 0 x 0 x 0 0 |
        | 0 0 0 x 0 0 0 |
        | 0 0 x 0 x 0 0 |
        | 0 x 0 0 0 x 0 |
        | x 0 0 0 0 0 x |
        -----------------
         */

        System.out.println("Please enter the size of the table");
        int size = TextIO.getInt();


        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                boolean firstRow = row == 0;
                boolean lastRow = row == size - 1;
                if (firstRow || lastRow) {
                    System.out.print("--");
                } else {
                    boolean firstColumn = col == 0;
                    boolean lastColumn = col == size - 1;
                    if (firstColumn || lastColumn) {
                        System.out.print("| ");
                    } else {
                        boolean mainDiagonal = row == col;
                        boolean sideDiagonal = col + row == size - 1;
                        if (mainDiagonal || sideDiagonal) {
                            System.out.print("x ");
                        } else {
                            System.out.print("0 ");
                        }
                    }
                }
            }
            System.out.println();
        }
    }
}
