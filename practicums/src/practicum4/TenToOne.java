package practicum4;

public class TenToOne {
    public static void main(String[] args) {
        //Print out numbers on one line from 10 to 1
        //10 9 8 7 6 5 4 3 2 1
        //Use a for loop
        //Think, what needs to be the beginning value for the loop
        //what needs to be the condition to end the loop
        //what needs to be done at the end of each iteration

        for (int i = 10; i > 0; i--) {
            System.out.print(i + " ");
        }
    }
}
