package practicum10;

import javafx.scene.shape.Circle;

/**
 * Created by mkunnapa on 31.10.2016.
 */
public class Enemy extends Circle {
    public Enemy(double radius) {
        this.setRadius(radius);
    }
}
