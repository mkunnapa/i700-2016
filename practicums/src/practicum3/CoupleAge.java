package practicum3;

import lib.TextIO;

public class CoupleAge {
    /*Write a program that asks the user for ages of a couple (lets the user insert two ages).
    The program then replies with how a couple with these ages is looked at. For instance:
    If the difference of ages is 5..10 years, the program tells "quite okay"
    If the difference of ages is 11..15 years, the program tells "not that okay"
    If the difference of ages is more than 15 years, the program tells "not okay"
    If the difference of ages is less than 5 years, the program tells "very nice" */

    public static void main(String[] args) {
        //get the age of the first person
        System.out.println("Please enter the age of the first person");
        int firstPersonAge = TextIO.getInt();
        //get the age of the second person
        System.out.println("Please enter the age of the second person");
        int secondPersonAge = TextIO.getInt();

        //get the difference of ages
        int differenceOfAges;
        if (firstPersonAge > secondPersonAge) {
            differenceOfAges = firstPersonAge - secondPersonAge;
        } else {
            differenceOfAges = secondPersonAge - firstPersonAge;
        }

        //alternative
        //int differenceOfAges = Math.abs(firstPersonAge - secondPersonAge);

        String outputText;

        if (differenceOfAges < 5) {
            outputText = "very nice";
        } else if (5 <= differenceOfAges && differenceOfAges <= 10) {
            outputText = "quite okay";
        } else if (11 <= differenceOfAges && differenceOfAges <= 15) {
            outputText = "not that okay";
        } else {
            outputText = "not okay";
        }

        System.out.println("That is " + outputText);

        //output the corresponding text
    }

}
