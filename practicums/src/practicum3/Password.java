package practicum3;

import lib.TextIO;

public class Password {
    //Write a program that asks user to insert a password. The program checks if the password is right and responds accordingly.
    // Keep in mind that double equation marks (==) cannot be used when checking the equality of Strings in Java, you must use String's "equals"-method.

    public static void main(String[] args) {
        //Store password
        String password = "password";
        boolean passwordIncorrect = true;
        String userInput;

        //Check if the password is correct
        while (passwordIncorrect) {
            //Prompt the user to input password
            System.out.println("Please enter the password: ");
            userInput = TextIO.getlnString();

            if (password.equals(userInput)) {
                passwordIncorrect = false;
            } else {
                System.out.println("Password incorrect");
            }

        }

        System.out.println("The password is correct");

    }
}

