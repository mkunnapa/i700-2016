package practicum3;

import lib.TextIO;

public class EvenOrOdd {

    public static void main(String[] args) {
        //Write a program that lets the user insert a number and prints out whether the number in odd or even.
        //get input from user
        System.out.println("Please enter a number");
        int userInput = TextIO.getlnInt();
        //check if the input number is even
        System.out.print("The number you entered is: ");
        if (userInput % 2 == 0) {
            System.out.println("even");
            //else print "even"
        } else {
            System.out.println("odd");
        }
    }
}
