package practicum9;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by mkunnapa on 24.10.2016.
 */
public class FileManager {
    public static void main(String[] args) {
       /* ArrayList<String> lines = readFile("readme.txt");
        for (String line : lines) {
            System.out.println(line);
        }*/
        ArrayList<String> linesToWrite = new ArrayList<>();
        linesToWrite.add("Hello");
        linesToWrite.add("What");
        linesToWrite.add("Are");
        linesToWrite.add("You");
        writeToFile(linesToWrite, "output.txt");
    }

    public static void writeToFile(ArrayList<String> linesToWrite, String fileName) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            for (String line : linesToWrite) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> readFile(String fileName) throws FileNotFoundException, IOException {
        ArrayList<String> lines = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        reader.close();
        return lines;
    }
}
