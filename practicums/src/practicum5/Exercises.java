package practicum5;

import lib.TextIO;

public class Exercises {
    public static void main(String[] args) {
//        System.out.println(xCharacters('c' , 4));
//        System.out.println(xCharacters('x' , 10));
//        System.out.println(xCharacters('a' , 3));

//        System.out.println(getNumber(1, 3));
/*        int number = getNumber(1, 1000);
        System.out.println("We are now in the main method");
        System.out.println(number);*/
        //System.out.println(getNumber("Enter number: ", 1, 5));
//        System.out.println(getNumber(1, 5));
//        for (int i = 0; i < 10; i++) {
//            System.out.println(random(-1,4));
//        }
        guessingGame(5);
    }

    public static int randomDemo() {
        //any random number in the range [0...1)
        double rand1 = Math.random();

        int cast = (int) Math.random();
        //any random number in the range [0...5]
        int result = (int) (Math.random() * 6);
        //any random number in the range [1...6]
        int result2 = (int) (Math.random() * 6) + 1;
        return result2;
    }

    public static String xCharacters(char c, int x) {
        String someStringValue = "";
        for (int i = 0; i < x; i++) {
            someStringValue += c;
        }
        return someStringValue;
    }

    public static int getNumber(int min, int max) {
        return getNumber("Hi, please enter a number", min, max);
    }

    public static int getNumber(String message, int min, int max) {
        return getNumber2(message, "Sorry, the number needs to be at least ", "Sorry, the number needs to be at most ", min, max);
    }

    public static int getNumber2(String enterNumber, String tooLow, String tooHigh, int min, int max) {
        while (true) {
            System.out.println(enterNumber);
            int input = TextIO.getInt();
            if (input < min) {
                System.out.println(tooLow + min);
            } else if (input > max) {
                System.out.println(tooHigh + max);
            } else {
                return input;
            }
        }
    }

    public static int random(int min, int max) {
        int range = max - min;
        int random = (int) (Math.random() * (range + 1)) + min;
        return random;
    }

    public static void guessingGame(int guesses) {
        int computerNumber = random(1, 100);
        for (int i = guesses; i > 0; i--) {
            if (guessGameLogic(guesses, computerNumber)) return;
            System.out.println("You have " + (guesses - 1) + " guesses remaining");
        }
        System.out.println("You have no lives remaining, game over!");
    }

    private static boolean guessGameLogic(int guesses, int computerNumber) {
        System.out.println("Insert a number between 1 and 100");
        int input = TextIO.getInt();
        if (input == computerNumber) {
            System.out.println("Congratulations, your guess was correct");
            System.out.println("You got it with " + guesses + " guesses remaining!");
            return true;
        } else if (input > computerNumber) {
            System.out.println("Your guess " + input + " was larger than the correct number");
        } else {
            System.out.println("Your guess " + input + " was smaller than the correct number");
        }
        return false;
    }
}
