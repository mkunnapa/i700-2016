package practicum7;

public class Human extends Object {

    private String name;
    private int age;

    public Human() {

    }

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greet() {
        System.out.println(greetAsString());
    }

    public String greetAsString() {
        //If the human is 3 years or older, the result should be "Hello, I am NAME and I am X years old."
        //If the human is less than 3 years old, the result should be "Boo boo"
        if (age < 3) {
            return "Boo boo";
        }
        return "Hello, I am " + name + " and I am " + age + " years old.";
    }

    @Override
    public String toString() {
        return name + ", " + age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
