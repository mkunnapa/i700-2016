package practicum7;

import lib.TextIO;

import java.util.ArrayList;

public class Greeter {
    public static void main(String[] args) {
        //Create an arrayList to store the humans
        ArrayList<Human> people = new ArrayList<>();

        //Get the user input
        //Ask for the name on one line and the age on next line, keep asking until the user enters an empty age
        while (true) {
            System.out.println("Please insert a name: ");
            String name = TextIO.getlnString();

            //End this while loop if the user entered an empty line
            if (name.isEmpty()) {
                break;
            }

            System.out.println("Please insert an age: ");
            int age = TextIO.getlnInt();

            //Use the variables to add some values to the ArrayList
            Human newHuman = new Human(name, age);
            people.add(newHuman);
        }

        //Make all the people greet you (For this you need to implement the Human.greet() method
        for (Human human : people) {
            human.greet();
        }
    }
}
