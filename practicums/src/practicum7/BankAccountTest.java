package practicum7;

import org.junit.Assert;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class BankAccountTest {

    @Test
    public void whenOpeningBankAccountMoneyAmountIsZero() {
        BankAccount account = new BankAccount();
        Assert.assertEquals(0., account.getBalance(), 0.001);
    }

    @Test
    public void whenDepositingMoneyTheBalanceIsIncreased() {
        BankAccount account = new BankAccount();
        double amountOfMoney = 10.;
        account.deposit(amountOfMoney);
        Assert.assertEquals(amountOfMoney , account.getBalance(), 0.001);
    }

    @Test
    public void whenWithdrawingMoneyTheBalanceIsDecreased() {
        BankAccount account = new BankAccount();
        double initialAmountOfMoney = 20.;
        account.deposit(initialAmountOfMoney);
        double amountOfMoney = 13.;
        account.withdraw(amountOfMoney);
        Assert.assertEquals(initialAmountOfMoney - amountOfMoney, account.getBalance(), 0.001);
    }

    @Test(expected = RuntimeException.class)
    public void whenWithdrawingMoneyFailsWhenNotEnoughOnTheAccount() throws RuntimeException {
        BankAccount account = new BankAccount();
        double initialAmountOfMoney = 20.;
        account.deposit(initialAmountOfMoney);
        double amountOfMoney = 100000000.;
        account.withdraw(amountOfMoney);
//        Assert.assertEquals(initialAmountOfMoney, account.getBalance(), 0.001);

    }

}