package practicum7;

import lib.TextIO;

public class Parantheses {
    public static void main(String[] args) {
//        parentheses();
        capitalCaseWithMinuses();
    }

    public static void parentheses() {
        System.out.println("Enter a line of text: ");
        String line = TextIO.getlnString();
        String[] words = line.split(" ");
        for (String word : words) {
            System.out.print("(" + word + ") ");
        }
        System.out.println();
    }


    public static void capitalCaseWithMinuses() {
        System.out.println("Enter a line of text: ");
        String line = TextIO.getlnString();
        String[] letters = line.split("");

        for (int i = 0; i < letters.length; i++) {
            System.out.print(letters[i].toUpperCase());
            if (i != letters.length -1) {
                System.out.print("-");
            }
        }
        System.out.println();
    }
}
