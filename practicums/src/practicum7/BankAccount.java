package practicum7;

public class BankAccount {
    private double money;


    public double getBalance() {
        return money;
    }

    public void deposit(double amountOfMoney) {
        money += amountOfMoney;
    }

    public void withdraw(double amountOfMoney) {
        if (amountOfMoney > money) {
            throw new RuntimeException("Not enough funds");
            //System.out.println("YOU CANNOT WITHDRAW THAT MUCH");
        } else {
            money -= amountOfMoney;
        }
    }
}
