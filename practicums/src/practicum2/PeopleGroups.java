package practicum2;

import lib.TextIO;

/**
 * Created by mkunnapa on 05.09.2016.
 */
public class PeopleGroups {

    public static void main(String[] args) {
        //User enters number of people
        System.out.println("Please enter the number of people: ");
        int numberOfPeople = TextIO.getInt();
        //User enters size of groups
        System.out.println("Please enter the size of groups: ");
        int sizeOfGroups = TextIO.getInt();

        //Tell the user how many full groups could be created
        //How many people were left over
        int numberOfGroups = numberOfPeople / sizeOfGroups;
        int peopleLeftOver = numberOfPeople % sizeOfGroups;

        System.out.println(numberOfGroups + " groups were created, " + peopleLeftOver + " were left over");
    }
}
