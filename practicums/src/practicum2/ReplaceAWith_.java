package practicum2;

import lib.TextIO;

/**
 * Created by mkunnapa on 05.09.2016.
 */
public class ReplaceAWith_ {
    public static void main(String[] args) {
        TextIO.putln("Enter input string");
        String input = TextIO.getlnString();
        String replaced = input.replace('a', '_');
        System.out.println(replaced);
    }
}
